package com.nacos.order.controller;

import com.nacos.order.clients.PaymentClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/16
 **/
@RestController
public class OrderController {

//OpenFeign远程调用
    @Autowired
    private PaymentClient paymentClient;

    @GetMapping("/order/{id}")
    public String openfeignTest(@PathVariable("id") Integer id){
        return paymentClient.test1(id);
    }

//下面代码，使用的是Ribbon 配合ResrTemplate完成远程调用
//    @Autowired
//    private RestTemplate restTemplate;
//    //调用的微服务名称
//    private final String url = "http://payment-server";
//    //远程调用支付服务，查看负载均衡效果
//    @GetMapping("/order/{id}")
//    public String paymentinfo(@PathVariable("id") Integer id){
//        return restTemplate.getForObject(url + "/payment/test1/" + id, String.class);
//    }
}
