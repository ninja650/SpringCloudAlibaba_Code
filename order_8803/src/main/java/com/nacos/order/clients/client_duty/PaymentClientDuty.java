package com.nacos.order.clients.client_duty;

import com.nacos.order.clients.PaymentClient;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/17
 **/

@Component
public class PaymentClientDuty implements PaymentClient {

    @Override
    public String test1(Integer id) {
        return "OpenFeign远程调用失败调用" + id;
    }
}
