package com.nacos.order.clients;

import com.nacos.order.clients.client_duty.PaymentClientDuty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "payment-server",fallback = PaymentClientDuty.class)
public interface PaymentClient {

    @GetMapping("/payment/test1/{id}")
    public String test1(@PathVariable("id") Integer id);
}
