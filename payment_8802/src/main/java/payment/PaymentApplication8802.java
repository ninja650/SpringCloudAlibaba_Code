package payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/12
 **/

@SpringBootApplication
@EnableDiscoveryClient
public class PaymentApplication8802 {
    public static void main(String[] args) {
        SpringApplication.run(PaymentApplication8802.class);
    }
}
