package com.ninja.seata.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/18
 **/

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonResult <T>  implements Serializable {
    private Integer statu;
    private String Messgae;
    private T data;

    public CommonResult(Integer statu, String messgae) {
        this.statu = statu;
        this.Messgae = messgae;
    }
}
