package com.ninja.seata.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/19
 **/
@Data
public class Order implements Serializable {

    private long id;
    private long userId;
    private long productId;
    private long count;
    private double money;
    private long status;
}
