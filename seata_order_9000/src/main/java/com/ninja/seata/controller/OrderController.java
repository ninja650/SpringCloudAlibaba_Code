package com.ninja.seata.controller;

import com.ninja.seata.domain.CommonResult;
import com.ninja.seata.domain.Order;
import com.ninja.seata.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Null;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/19
 **/

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    //便于测试，我们只穿数量和金额就可以了，使用get
    @GetMapping("/create/{count}/{money}")
    public CommonResult createOrder(@PathVariable("count") Long count, @PathVariable("money") Double money) {
        //自定义订单，模拟下单数据
        Order order = new Order();
        order.setCount(count);
        order.setMoney(money);
        order.setProductId(1); //商品表准备好这个商品
        order.setUserId(1);  //账户表也准备好这个账户
        Long resultCode = orderService.createOrder(order);
        if (resultCode == null){
            return new CommonResult<>(444, "新增订单失败");
        }
        return new CommonResult<>(200, "新增订单成功",resultCode);
    }
}
