package com.ninja.seata.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "seata-account-server")
public interface AccountClient {

    //减余额
    @GetMapping("/account/reduce/{userId}/{money}")
    public void reduce(@PathVariable("userId") Long userId, @PathVariable("money") Double money);
}
