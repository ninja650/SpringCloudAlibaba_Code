package com.ninja.seata.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "seata-storage-server")
public interface StorageClient {

    //减库存
    @GetMapping("/storage/reduce/{id}/{num}")
    public void reduce(@PathVariable("id") Long id, @PathVariable("num") Long num);
}
