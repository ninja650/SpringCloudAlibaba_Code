package com.ninja.seata;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/18
 **/

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableFeignClients(basePackages = "com.ninja.seata.client")
@EnableDiscoveryClient
@MapperScan(value = "com.ninja.seata.mapper")
public class OrderApplication9000 {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication9000.class);
    }
}
