package com.ninja.seata.mapper;

import com.ninja.seata.domain.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OrderMapper {
    //添加一个订单，并返回订单id
    void addOrder(Order order);
    //修改订单的状态为 1
    void updateStatus(@Param("id") long id);
}
