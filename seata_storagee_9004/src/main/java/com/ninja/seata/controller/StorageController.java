package com.ninja.seata.controller;

import com.ninja.seata.mapper.StorageMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/19
 **/

@RestController
@RequestMapping("/storage")
@Slf4j
public class StorageController {

    @Autowired
    private StorageMapper storageMapper;

    @GetMapping("/reduce/{id}/{num}")
    public void reduce(@PathVariable("id")Long id,@PathVariable("num")Long num){
        storageMapper.redecu(id,num);
        log.info(id + "商品，扣取库存为:" + num);
    }
}
