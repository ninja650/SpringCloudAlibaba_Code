package com.ninja.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/16
 **/

@SpringBootApplication
@EnableDiscoveryClient
public class NacosConfigAllication {
    public static void main(String[] args) {
        SpringApplication.run(NacosConfigAllication.class);
    }
}
