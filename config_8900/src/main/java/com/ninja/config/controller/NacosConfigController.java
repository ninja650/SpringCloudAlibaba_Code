package com.ninja.config.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/16
 **/

@RestController
@RefreshScope  //支持nacos作为config配置中心的动态刷新
public class NacosConfigController {

    //读取配置中心的数据
    @Value("${config.info}")
    private String info;

    //将配置中心读取到的配置暴露出去测试一下
    @GetMapping("/config/info")
    public String  test1(){
        return info;
    }

}
