package com.ninja.account.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AccountMapper {
    void reduce(@Param("userId") long userId, @Param("money") double money);
}
