package com.ninja.account.controller;

import com.ninja.account.mapper.AccountMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.web.bind.annotation.*;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/19
 **/

@RequestMapping("/account")
@RestController
@Slf4j
public class AccountController {

    @Autowired
    private AccountMapper accountMapper;

    @GetMapping("/reduce/{userId}/{money}")
    public void reduce(@PathVariable("userId")Long userId, @PathVariable("money")Double money){
//        int i = 10 / 0;
        accountMapper.reduce(userId,money);
        log.info(userId + "用户，扣除" + money + "余额");
    }
}
