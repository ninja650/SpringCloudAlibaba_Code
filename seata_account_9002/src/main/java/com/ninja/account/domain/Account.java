package com.ninja.account.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/19
 **/

@Data
public class Account implements Serializable {
    private long id;
    private long userId;
    private double total;
    private double residue;
}
