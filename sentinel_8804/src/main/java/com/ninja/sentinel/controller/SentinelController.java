package com.ninja.sentinel.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalTime;

/**
 * @Description  sentinel是懒加载，需要访问一下下面的接口才可以开始监控
 * @Author Ninja
 * @Date 2020/6/16
 **/

@RestController

public class SentinelController {

    @GetMapping("/test1")
    public String test1() {
        System.out.println( LocalTime.now() +  Thread.currentThread().getName() + "正在处理请求");
        return "This is test1";
    }

    @GetMapping("/test2")
    public String test2() throws InterruptedException {
        Thread.sleep(1000);
        System.out.println("测试RT");
        return "This is test2";
    }

    @GetMapping("/test3")
    public String test3() throws InterruptedException {
        System.out.println("测试异常比例");
        int i = 10 / 0;
        return "This is test3";
    }

    @GetMapping("/test4")
    @SentinelResource(value = "test4",blockHandler = "duty_test4")
    public String test4(@RequestParam(value = "p1" ,required = false)String p1,
                        @RequestParam(value = "p2" ,required = false)String p2) {
        System.out.println("测试热点参数" + "--" +p1 + "--" + p2);
        return "This is test4";
    }


    //test4de 兜底方法，现在我们不用Sentinel默认的那个提示了，用自定义的
    public String duty_test4(String p1, String p2, BlockException e){
        System.out.println( p1 + p2);
        return "好像出了点问题";
    }

}
