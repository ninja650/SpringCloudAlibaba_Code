package com.ninja.payment.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/16
 **/

@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Value("${server.port}")
    private String port;

    @GetMapping("/test1/{id}")
    public String test1(@PathVariable("id") Integer id){
        return "This is Payment server,Port is " + port + "\t id为" + id;
     }
}
