package com.ninja.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Description
 * @Author Ninja
 * @Date 2020/6/16
 **/

@SpringBootApplication
@EnableDiscoveryClient
public class PaymentApplication8801 {
    public static void main(String[] args) {
        SpringApplication.run(PaymentApplication8801.class);
    }
}
